<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <!--
        This website is powered by TYPO3 - inspiring people to share!
        TYPO3 is a free open source Content Management Framework initially created by Kasper Skaarhoj and licensed under GNU/GPL.
        TYPO3 is copyright 1998-2016 of Kasper Skaarhoj. Extensions are copyright of their respective owners.
        Information and contribution at http://typo3.org/
    -->


{{--    <link rel="shortcut icon" href="{{asset('files/favicon.ico')}}" type="image/x-icon; charset=binary">--}}
{{--    <link rel="icon" href="{{asset('files/favicon.ico')}}" type="image/x-icon; charset=binary">--}}
    <title>SHAGA MIX-LLC @yield('title')</title>
    <meta name="generator" content="GGP">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">


    <link rel="stylesheet" type="text/css" href="{{asset('css/cbb.css')}}" media="all">
    <link rel="stylesheet" type="text/css" href="{{asset('css/cssPart_1.css')}}" media="all">
    <link rel="stylesheet" type="text/css"
          href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" media="all">
    <link rel="stylesheet" type="text/css" href="{{asset('css/cssPart_2.css')}}" media="all">
    <link rel="stylesheet" type="text/css" href="{{asset('css/cssPart_3.css')}}" media="all">
    <link rel="stylesheet" type="text/css" href="{{asset('css/cssPart_4.min.css')}}" media="all">
    <link rel="stylesheet" type="text/css" href="{{asset('css/cookies.css')}}" media="all">


    <script src="{{asset('js/jquery.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/jquery.validate.min.js')}}" type="text/javascript"></script>

    <link rel="canonical" href="{{config('app.url')}}">
{{--    <link rel="apple-touch-icon" sizes="57x57" href="{{asset('files//apple-icon-57x57.png')}}">--}}
{{--    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('files/apple-icon-60x60.png')}}">--}}
{{--    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('files/apple-icon-72x72.png')}}">--}}
{{--    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('files/apple-icon-76x76.png')}}">--}}
{{--    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('files/apple-icon-114x114.png')}}">--}}
{{--    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('files/apple-icon-120x120.png')}}">--}}
{{--    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('files/apple-icon-144x144.png')}}">--}}
{{--    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('files/apple-icon-152x152.png')}}">--}}
{{--    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('files/apple-icon-180x180.png')}}">--}}
{{--    <link rel="icon" type="image/png" sizes="192x192" href="{{asset('files/android-icon-192x192.png')}}">--}}
{{--    <link rel="manifest" href="{{asset('files/manifest.json')}}">--}}
{{--    <meta name="msapplication-TileColor" content="#ffffff">--}}
{{--    <meta name="msapplication-TileImage" content="{{asset('files/ms-icon-144x144.png')}}">--}}
{{--    <meta name="theme-color" content="#ffffff">--}}
    @yield('css')

</head>
<body class="p-layout-0" id="page-61">

<div class="mask-l"></div>
<header>
    <div class="b-top-options-panel">
        <div class="container">

            <nav class="b-meta-nav">
                <ul>
                    <li class="meta-nav_1level"><a href="{{route('index').'#contact_part'}}" title="Contact">Contact</a>
                    </li>
                </ul>
            </nav>
            <nav class="b-social-nav hidden-md hidden-sm">
                <ul>
                    <li><a href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>
                    </li>
                    <li><a href="https://www.facebook.com/" target="_blank"><i
                                class="fa fa-facebook"></i></a></li>
                    <li><a href="https://www.xing.com/" target="_blank"><i
                                class="fa fa-xing"></i></a></li>
                    <li>
                        <a href="https://www.linkedin.com/"
                           target="_blank"><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="container b-header__box b-relative">
        <a href="{{route('index')}}" class="b-left b-logo ">
            <h1 style="width: 240px">SHAGA MIX</h1>
{{--            <img class="b-logo-default" data-retina src="{{asset('files/logo-header-default-en.png')}}" width="188"--}}
{{--                 alt="SHAGA MIX-llc"/>--}}
{{--            <img class="b-logo-small" data-retina src="{{asset('files/logo-header-default-en.png')}}" width="188"--}}
{{--                 alt="SHAGA MIX-llc"/>--}}
        </a>
        <div class="b-header-r b-right b-header-r--icon">
            <div class="b-top-nav-show-slide f-top-nav-show-slide b-right j-top-nav-show-slide">
                <i class="fa fa-align-justify"></i>
            </div>
            <nav class="b-top-nav f-top-nav b-right j-top-nav">

                <div class="mobile-meta-nav">
                    <ul>
                        <li class="meta-nav_1level"><a href="{{route('index').'#contact_part'}}" title="Contact">Contact</a>
                        </li>
                    </ul>
                </div>

            </nav>
        </div>
    </div>
</header>
<div class="j-menu-container"></div>
<div class="l-main-container"></div>
@yield('body')
<!--TYPO3SEARCH_end-->

<footer class="footer">
    <div class="b-footer-primary">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-xs-12 f-copyright b-copyright">
                </div>
                <div class="col-sm-8 col-xs-12">
                    <div class="b-btn f-btn b-btn-default b-right b-footer__btn_up f-footer__btn_up j-footer__btn_up">
                        <i class="fa fa-chevron-up"></i>
                    </div>
                    <nav class="b-bottom-nav f-bottom-nav b-right">
                        <ul>
                            <li><a href="{{route('index')}}" title="SHAGA MIX">SHAGA MIX</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="b-footer-secondary row">
            <div class="col-md-2">
                <div class="h4" style="font-family: Rotis Serif W01 Bold,serif;font-size: 1.615em;"></div>
                <!--Jobs und Karriere--></div>
            <div class="col-md-2">
                <div class="h4" style="font-family: Rotis Serif W01 Bold,serif;font-size: 1.615em;"></div>
                <!--Für Unternehmen--></div>
            <div class="col-md-2">
                <div class="h4" style="font-family: Rotis Serif W01 Bold,serif;font-size: 1.615em;"></div>
                <!--SHAGA MIX--></div>
            <div class="col-md-2">
                <div class="h4" style="font-family: Rotis Serif W01 Bold,serif;font-size: 1.615em;"></div>
            </div>
            <div class="col-md-3" id="contact_part">
                <div id="c1957" class="csc-default">
                    <div class="h4" style="font-family: Rotis Serif W01 Bold,serif;font-size: 1.615em;">Contact</div>

                    <div class="b-contacts-short-item-group">
                        <div class="b-contacts-short-item col-md-12 col-sm-4 col-xs-12">
                            <div
                                class="b-contacts-short-item__icon f-contacts-short-item__icon f-contacts-short-item__icon_lg b-left">
                                <i class="icon-essential-regular-88-pin"></i>
                            </div>
                            <div class="b-remaining f-contacts-short-item__text">
                                <span class="f-primary-sb">SHAGA MIX DWC LLC</span><br>
                                Dubai World Center
                                (Business center)<br>
                                P. O. Box:390667<br>
                                Dubai UAE<br>
                            </div>
                        </div>
{{--                        <div class="b-contacts-short-item col-md-12 col-sm-4 col-xs-12">--}}
{{--                            <div--}}
{{--                                class="b-contacts-short-item__icon f-contacts-short-item__icon b-left f-contacts-short-item__icon_md">--}}
{{--                                <i class="icon-essential-regular-44-phone"></i>--}}
{{--                            </div>--}}
{{--                            <div class="b-remaining f-contacts-short-item__text f-contacts-short-item__text_phone">--}}
{{--                                Phone: (+49) 69 96876-0<br>--}}
{{--                                Fax: (+49) 69 96876-399--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <div class="b-contacts-short-item col-md-12 col-sm-4 col-xs-12">
                            <div
                                class="b-contacts-short-item__icon f-contacts-short-item__icon b-left f-contacts-short-item__icon_xs">
                                <i class="icon-essential-regular-27-envelope"></i>
                            </div>
                            <div class="b-remaining f-contacts-short-item__text f-contacts-short-item__text_email">
                                <a href="mailto:info@shagamix.com">info@shagamix.com</a>
                            </div>
                        </div>
                    </div>
                    <div class="f-center">
                        <a href="{{route('index')}}"><h2>SHAGA MIX</h2></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="{{asset('js/4d460472-f83d-45e7-a771-35d87caad9f0.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jsPart_1.min.js')}}" type="text/javascript"></script>
<script
    src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1','packages':['corechart']}]}"
    type="text/javascript"></script>
<script src="{{asset('js/video.js')}}"
        type="text/javascript"></script>
<script src="{{asset('js/jquery-ui.min.js')}}"
        type="text/javascript"></script>
<script src="https://maps.google.com/maps/api/js?sensor=false&amp;key=AIzaSyCwYJIENwVRKHbGl_ak3xyNFCfCxDqHAHc"
        type="text/javascript"></script>
<script src="{{asset('js/jsPart_2.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jsPart_3.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jsPart_4.min.js')}}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.4.13/d3.min.js" type="text/javascript"></script>

@yield('js')

</body>
</html>

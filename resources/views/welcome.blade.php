@extends('layout.app')
@section('title', ' | Home')
@section('css')
    @endsection
@section('body')



<!--TYPO3SEARCH_begin-->
<div id="c814" class="csc-default">
    <div class="tx-dce-pi1">

        <div class="b-slidercontainer">
            <div class="b-slider j-fullscreenslider">
                <ul>


                    <li data-transition="slidehorizontal" data-slotamount="7" data-masterspeed="1500"
                        data-fstransition="fade" data-fsmasterspeed="300" data-adaptiveHeight="true"
                        data-fsslotamount="7">

                        <img data-retina src="{{asset('files/we-strenghten.jpg')}}" alt="">
                        <div class="caption fade " data-x="left" data-y="106" data-speed="600" data-start="2600">
                            <div class="h1">
                                <span class="cite-author"></span>
                            </div>
                        </div>

                        <div class="caption fade " data-x="left" data-y="315" data-speed="800" data-start="2600">
                            <a class="button-lg sliderButtonEvent" href="{{route('index').'#contact_part'}}">Contact us</a>
                        </div>
                    </li>

                </ul>
                <div class="slider-shadow"></div>
            </div>
        </div>

    </div>
</div>
<div id="c815" class="csc-default">
    <div class="tx-dce-pi1">

        <div class="b-breadcrumbs f-breadcrumbs">
            <div class="container">
                <ul>
                    <li class="home"><a href="/"><i class="icon-essential-regular-41-home"></i>Home</a></li>
                </ul>
            </div>
        </div>

    </div>
</div>
<div id="c813" class="csc-default">
    <div class="tx-dce-pi1">

        <section class="b-section-info content-module">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-xs-12">
                        <h2>Our candidates are your perfect line-up.</h2>
                        <div class="h4">Experienced experts with the latest expertise are waiting to work with you.
                        </div>
                        <p class="bodytext">SHAGA MIX has specialised in the commercial and the IT environments
                            and its services focus exclusively on these segments. This specialisation
                            makes us experts who can advise you in the  Software Development, Marketing, Project Management  and IT-Services
                            divisions with the required expertise and provide tailored support with competent
                            candidates. </p>
                        <p class="bodytext">Reap the benefits of our technical expertise and the quality of our
                            employees, who have sound commercial and IT-specific training and/or business or computer
                            science degrees. We also insist that all our candidates are experienced MS Office users with
                            intermediate to advanced foreign language skills. </p>
                        <p class="bodytext"><b>Partnership is paramount</b></p>
                        <p class="bodytext">Our consultants rely on personal interaction with our customers and strive
                            for long-term strategic partnerships to ensure our common success. This personal contact
                            allows us to identify any internal issues which make it harder for customers to find and
                            recruit talent. Both during and after the temporary placement of employees and following
                            successful permanent placements, we contact our customers personally to ask how satisfied
                            they are. We also take time to plan personnel deployment for future assignments and projects
                            together with our customers.</p>
                        <p class="bodytext">In addition to personal consultation, we set new standards of service for
                            our customers through customised events. This knowledge sharing on topics relating to labour
                            law, tax, accounting and financial controls as well as IT aims at building on the
                            cooperative collaboration we enjoy with our customers.</p>

                    </div>
                    <div class="col-sm-4 col-xs-12 cm-sidebar">


                        <div class="sidebar-nav">
                            <div class="h4 f-primary-b">Further Information</div>
                            <nav>
                                <ul>


                                    <li><a class="f-primary-sb" href="{{route('index').'#contact_part'}}"><i
                                                class="icon-essential-regular-07-arrow-right"></i>Contact us</a></li>


                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
</div>
{{--<div id="c816" class="csc-default">--}}
{{--    <div class="tx-dce-pi1">--}}


{{--        <section class="b-section-info argument-module">--}}
{{--            <div class="container">--}}
{{--                <h2 class="f-center">Our strenghts. Your benefits.</h2>--}}
{{--                <p class="f-center f-primary subline-description">We will provide you with the ideal personnel service--}}
{{--                    and candidate for your needs - quick, reliable and efficient.</p>--}}
{{--                <div class="b-infoblock-with-icon-group row b-infoblock-with-icon--circle-icon">--}}
{{--                    <div class="col-md-4 col-sm-12 col-xs-12">--}}
{{--                        <div class="b-infoblock-with-icon">--}}


{{--										<span--}}
{{--                                            class="b-infoblock-with-icon__icon f-infoblock-with-icon__icon fade-in-animate">--}}
{{--											<img class="fade-in-animate from-list" data-retina data-animate="fadeInLeft"--}}
{{--                                                 src="{{asset('files/anforderungen.svg')}}" alt="Argument Icon"/>--}}
{{--										</span>--}}


{{--                            <div class="b-infoblock-with-icon__info">--}}

{{--                                <span--}}
{{--                                    class="f-infoblock-with-icon__info_title b-infoblock-with-icon__info_title f-primary-sb">Ideal candidates for your vacancies.</span>--}}


{{--                                <div class="f-infoblock-with-icon__info_text b-infoblock-with-icon__info_text">--}}
{{--                                    Custom-fit placement of your vacancies through consequent specialisation in the--}}
{{--                                    commercial sphere and IT sector and a qualified needs analysis.--}}
{{--                                </div>--}}

{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-4 col-sm-12 col-xs-12">--}}
{{--                        <div class="b-infoblock-with-icon">--}}


{{--										<span--}}
{{--                                            class="b-infoblock-with-icon__icon f-infoblock-with-icon__icon fade-in-animate">--}}
{{--											<img class="fade-in-animate from-list" data-retina data-animate="fadeInLeft"--}}
{{--                                                 src="{{asset('files/lupe.svg')}}" alt="Argument Icon"/>--}}
{{--										</span>--}}


{{--                            <div class="b-infoblock-with-icon__info">--}}

{{--                                <span--}}
{{--                                    class="f-infoblock-with-icon__info_title b-infoblock-with-icon__info_title f-primary-sb">Fast recruitment of experts and managers.</span>--}}


{{--                                <div class="f-infoblock-with-icon__info_text b-infoblock-with-icon__info_text">--}}
{{--                                    We are able to find the right employee for you within a few days. Our flexible--}}
{{--                                    employees and acknowledged experts are able to support your company quickly and--}}
{{--                                    effectively.--}}
{{--                                </div>--}}

{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-4 col-sm-12 col-xs-12">--}}
{{--                        <div class="b-infoblock-with-icon">--}}


{{--										<span--}}
{{--                                            class="b-infoblock-with-icon__icon f-infoblock-with-icon__icon fade-in-animate">--}}
{{--											<img class="fade-in-animate from-list" data-retina data-animate="fadeInLeft"--}}
{{--                                                 src="{{asset('files/siegel.svg')}}" alt="Argument Icon"/>--}}
{{--										</span>--}}


{{--                            <div class="b-infoblock-with-icon__info">--}}

{{--                                <span--}}
{{--                                    class="f-infoblock-with-icon__info_title b-infoblock-with-icon__info_title f-primary-sb">Efficient recruiting expertise at your side.</span>--}}


{{--                                <div class="f-infoblock-with-icon__info_text b-infoblock-with-icon__info_text">--}}
{{--                                    You benefit from our professional expertise and the manifold recruiting measures--}}
{{--                                    which spare you time, expenses and resources.--}}
{{--                                </div>--}}

{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="b-infoblock-with-icon-group row b-infoblock-with-icon--circle-icon">--}}
{{--                    <div class="col-md-4 col-sm-12 col-xs-12">--}}
{{--                        <div class="b-infoblock-with-icon">--}}


{{--											<span--}}
{{--                                                class="b-infoblock-with-icon__icon f-infoblock-with-icon__icon fade-in-animate">--}}
{{--												<img class="fade-in-animate from-list" data-retina--}}
{{--                                                     data-animate="fadeInLeft" src="{{asset('files/kontakt.svg')}}"--}}
{{--                                                     alt="Argument Icon"/>--}}
{{--											</span>--}}


{{--                            <div class="b-infoblock-with-icon__info">--}}

{{--                                <span--}}
{{--                                    class="f-infoblock-with-icon__info_title b-infoblock-with-icon__info_title f-primary-sb">The personal contact counts. The partnership decides.</span>--}}


{{--                                <div class="f-infoblock-with-icon__info_text b-infoblock-with-icon__info_text">--}}
{{--                                    We aim to establish a long-term, cooperative partnership for sustainable success.--}}
{{--                                    Continuous satisfaction surveying and personal meetings to talk about future--}}
{{--                                    personnel deployments are the fundament of our work.--}}
{{--                                </div>--}}

{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-4 col-sm-12 col-xs-12">--}}
{{--                        <div class="b-infoblock-with-icon">--}}


{{--											<span--}}
{{--                                                class="b-infoblock-with-icon__icon f-infoblock-with-icon__icon fade-in-animate">--}}
{{--												<img class="fade-in-animate from-list" data-retina--}}
{{--                                                     data-animate="fadeInLeft" src="{{asset('files/unternehmen.svg')}}"--}}
{{--                                                     alt="Argument Icon"/>--}}
{{--											</span>--}}


{{--                            <div class="b-infoblock-with-icon__info">--}}

{{--                                <span--}}
{{--                                    class="f-infoblock-with-icon__info_title b-infoblock-with-icon__info_title f-primary-sb">Unique service portfolio.</span>--}}


{{--                                <div class="f-infoblock-with-icon__info_text b-infoblock-with-icon__info_text">--}}
{{--                                    Our service portfolio consisting of personnel services and training possibilities--}}
{{--                                    for up to date expert knowledge is unique in the German market.--}}
{{--                                </div>--}}

{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-4 col-sm-12 col-xs-12">--}}
{{--                        <div class="b-infoblock-with-icon">--}}
{{--                            <div class="b-infoblock-with-icon__info">--}}

{{--                                <span--}}
{{--                                    class="f-infoblock-with-icon__info_title b-infoblock-with-icon__info_title f-primary-sb"></span>--}}


{{--                                <div class="f-infoblock-with-icon__info_text b-infoblock-with-icon__info_text">--}}

{{--                                </div>--}}

{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--            </div>--}}
{{--        </section>--}}


{{--    </div>--}}
{{--</div>--}}
{{--<div id="c817" class="csc-default">--}}
{{--    <div class="tx-dce-pi1">--}}

{{--        <section class="b-section-info teaser-module-images b-bg-default-color-3">--}}
{{--            <div class="container">--}}

{{--                <div class="row teaser-module-headline">--}}
{{--                    <div class="col-md-12">--}}

{{--                        <h1 class="f-center">The partner at your side: For tailored personnel services</h1>--}}


{{--                        <p class="f-center f-primary subline-description">In addition to custom-fit candidates, we offer--}}
{{--                            you in each case the personnel service that suits the current requirements in your--}}
{{--                            organisation the best:</p>--}}

{{--                    </div>--}}
{{--                </div>--}}


{{--                <div class="row">--}}


{{--                    <div class="col-md-4 col-sm-6">--}}

{{--                        <div class="teaser-box">--}}

{{--                            <div class="teaser-image">--}}
{{--                                <a href="{{route('index').'#contact_part'}}"><img class="fade-in-animate"--}}
{{--                                                                            src="{{asset('files/teaser-bewerbung-1_34.jpg')}}"--}}
{{--                                                                            alt="Specialised temporary staffing: Contact us"/></a>--}}
{{--                            </div>--}}

{{--                            <div class="teaser-box-content">--}}
{{--                                <div class="teaser-box-content-text">--}}
{{--                                    <h4 class="f-primary-b">Specialised temporary staffing</h4>--}}
{{--                                    <p>--}}
{{--                                    <p class="bodytext"><b>React to changes within your company and market at short--}}
{{--                                            notice.</b></p>--}}
{{--                                    <p class="bodytext">You appoint our temporary workers for the replacement of your--}}
{{--                                        employees, the additional time-wise support or the testing of employees for a--}}
{{--                                        permanent position (temp-to-perm). By using our specialised temporary staffing,--}}
{{--                                        you will reduce the administrative costs in the context of recruitment--}}
{{--                                        processes. Moreover, you just pay for the actual work hours of our employee.--}}
{{--                                        Thus, you can calculate the personnel costs of the deployed temporary worker--}}
{{--                                        exactly and always keep track of them.</p></p>--}}
{{--                                </div>--}}

{{--                                <a class="more" href="{{route('index').'#contact_part'}}"><i--}}
{{--                                        class="icon-essential-regular-07-arrow-right"></i><span class="f-primary-sb">Contact us</span></a>--}}

{{--                            </div>--}}
{{--                        </div>--}}

{{--                    </div>--}}
{{--                    <div class="col-md-4 col-sm-6">--}}

{{--                        <div class="teaser-box">--}}

{{--                            <div class="teaser-image">--}}
{{--                                <a href="{{route('index').'#contact_part'}}"><img class="fade-in-animate"--}}
{{--                                                                            src="{{asset('files/teaser-bewerbung-2_34.jpg')}}"--}}
{{--                                                                            alt="Permanent placement: Contact us"/></a>--}}
{{--                            </div>--}}

{{--                            <div class="teaser-box-content">--}}
{{--                                <div class="teaser-box-content-text">--}}
{{--                                    <h4 class="f-primary-b">Permanent placement</h4>--}}
{{--                                    <p>--}}
{{--                                    <p class="bodytext"><b>Right personnel decision. Savings of time, money and--}}
{{--                                            resources.</b></p>--}}
{{--                                    <p class="bodytext">We are pleased to take over the complete recruitment process of--}}
{{--                                        experts and managers for permanent positions for you. You can use our recruiting--}}
{{--                                        expertise in the commercial sphere and IT sector to avoid cost intensive--}}
{{--                                        misappointments.</p>--}}
{{--                                    <p class="bodytext">Your request is free of charge. Costs will only occur in case of--}}
{{--                                        a successful placement of our candidate.</p></p>--}}
{{--                                </div>--}}

{{--                                <a class="more" href="{{route('index').'#contact_part'}}"><i--}}
{{--                                        class="icon-essential-regular-07-arrow-right"></i><span class="f-primary-sb">Contact us</span></a>--}}

{{--                            </div>--}}
{{--                        </div>--}}

{{--                    </div>--}}
{{--                    <div class="col-md-4 col-sm-6">--}}

{{--                        <div class="teaser-box">--}}

{{--                            <div class="teaser-image">--}}
{{--                                <a href="{{route('index').'#contact_part'}}"><img class="fade-in-animate"--}}
{{--                                                                            src="{{asset('files/teaser-bewerbung-3_34.jpg')}}"--}}
{{--                                                                            alt="Interim management: Contact us"/></a>--}}
{{--                            </div>--}}

{{--                            <div class="teaser-box-content">--}}
{{--                                <div class="teaser-box-content-text">--}}
{{--                                    <h4 class="f-primary-b">Interim management</h4>--}}
{{--                                    <p>--}}
{{--                                    <p class="bodytext"><b>Experts for a period and managers on call.</b></p>--}}
{{--                                    <p class="bodytext">We place our interim managers in your company if your staff--}}
{{--                                        capacity is insufficient or subject-specific know-how is needed. Our interim--}}
{{--                                        managers are able to bridge management shortages and resolve challenging company--}}
{{--                                        situations.</p>--}}
{{--                                    <p class="bodytext">You will benefit from their external skills with specialist and--}}
{{--                                        industry knowledge, a results-oriented implementation and calculable--}}
{{--                                        remunerations through the use of transparent contracts.</p></p>--}}
{{--                                </div>--}}

{{--                                <a class="more" href="{{route('index').'#contact_part'}}"><i--}}
{{--                                        class="icon-essential-regular-07-arrow-right"></i><span class="f-primary-sb">Contact us</span></a>--}}

{{--                            </div>--}}
{{--                        </div>--}}

{{--                    </div>--}}
{{--                </div>--}}


{{--            </div>--}}
{{--            <script>--}}
{{--                $(document).ready(function () {--}}
{{--                    maxHeightBox = 0;--}}
{{--                    $('.teaser-box-content-text').each(function () {--}}
{{--                        if (maxHeightBox < $(this).height()) maxHeightBox = $(this).height();--}}
{{--                    });--}}
{{--                    $('.teaser-box-content-text').height(maxHeightBox);--}}


{{--                    $('.teaser-box-content').each(function () {--}}
{{--                        if (maxHeightBox < $(this).height()) maxHeightBox = $(this).height();--}}
{{--                    });--}}
{{--                    $('.teaser-box-content').height(maxHeightBox);--}}

{{--                });--}}

{{--            </script>--}}
{{--        </section>--}}

{{--    </div>--}}
{{--</div>--}}
<div id="c2030" class="csc-default">
    <div class="tx-dce-pi1">


        <section class="b-section-info argument-module">
            <div class="container">
                <h2 class="f-center">Our specialisation</h2>
                <p class="f-center f-primary subline-description">The main areas for deployment of our experts and
                    managers.</p>
                <div class="b-infoblock-with-icon-group row b-infoblock-with-icon--circle-icon">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="b-infoblock-with-icon">
                            <a href="{{route('index').'#contact_part'}}">

											<span
                                                class="b-infoblock-with-icon__icon f-infoblock-with-icon__icon fade-in-animate">
												<img class="fade-in-animate from-list" data-retina
                                                     data-animate="fadeInLeft"
                                                     src="{{asset('files/it_service.svg')}}"
                                                     alt="Argument Icon"/>
											</span>

                            </a>
                            <div class="b-infoblock-with-icon__info">

                                <a href="{{route('index').'#contact_part'}}"><span
                                        class="f-infoblock-with-icon__info_title b-infoblock-with-icon__info_title f-primary-sb">IT Services, Computer Software Trading & Project Management</span></a>


                                <div class="f-infoblock-with-icon__info_text b-infoblock-with-icon__info_text">
                                    Help desk and technical support; Software and web development; System and network
                                    administration; Database development; Project management; Consulting
                                </div>

                                <a class="more" href="{{route('index').'#contact_part'}}"><i
                                        class="icon-essential-regular-07-arrow-right"></i><span class="f-primary-sb">Contact us for further information</span></a>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="b-infoblock-with-icon">
                            <a href="{{route('index').'#contact_part'}}">

										<span
                                            class="b-infoblock-with-icon__icon f-infoblock-with-icon__icon fade-in-animate">
											<img class="fade-in-animate from-list" data-retina data-animate="fadeInLeft"
                                                 src="{{asset('files/account.svg')}}" alt="Argument Icon"/>
										</span>

                            </a>
                            <div class="b-infoblock-with-icon__info">

                                <a href="{{route('index').'#contact_part'}}"><span
                                        class="f-infoblock-with-icon__info_title b-infoblock-with-icon__info_title f-primary-sb">Marketing services</span></a>


                                <div class="f-infoblock-with-icon__info_text b-infoblock-with-icon__info_text">
                                    Strategic Marketing, Market Research and Consultancy, MarComm , PR and Advertising.
                                </div>

                                <a class="more" href="{{route('index').'#contact_part'}}"><i
                                        class="icon-essential-regular-07-arrow-right"></i><span class="f-primary-sb">Contact us for further information</span></a>

                            </div>
                        </div>
                    </div>
                </div>

{{--                <div class="b-infoblock-with-icon-group row b-infoblock-with-icon--circle-icon">--}}
{{--                    <div class="col-md-4 col-sm-12 col-xs-12">--}}
{{--                        <div class="b-infoblock-with-icon">--}}
{{--                            <a href="{{route('index').'#contact_part'}}">--}}

{{--											<span--}}
{{--                                                class="b-infoblock-with-icon__icon f-infoblock-with-icon__icon fade-in-animate">--}}
{{--												<img class="fade-in-animate from-list" data-retina--}}
{{--                                                     data-animate="fadeInLeft"--}}
{{--                                                     src="{{asset('files/it_service.svg')}}"--}}
{{--                                                     alt="Argument Icon"/>--}}
{{--											</span>--}}

{{--                            </a>--}}
{{--                            <div class="b-infoblock-with-icon__info">--}}

{{--                                <a href="{{route('index').'#contact_part'}}"><span--}}
{{--                                        class="f-infoblock-with-icon__info_title b-infoblock-with-icon__info_title f-primary-sb">IT Services & Computer Software Trading</span></a>--}}


{{--                                <div class="f-infoblock-with-icon__info_text b-infoblock-with-icon__info_text">--}}
{{--                                    Help desk and technical support; Software and web development; System and network--}}
{{--                                    administration; Database development; Project management; Consulting--}}
{{--                                </div>--}}

{{--                                <a class="more" href="{{route('index').'#contact_part'}}"><i--}}
{{--                                        class="icon-essential-regular-07-arrow-right"></i><span class="f-primary-sb">Contact us for further information</span></a>--}}

{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-4 col-sm-12 col-xs-12">--}}
{{--                        <div class="b-infoblock-with-icon">--}}
{{--                            <div class="b-infoblock-with-icon__info">--}}

{{--                                <span--}}
{{--                                    class="f-infoblock-with-icon__info_title b-infoblock-with-icon__info_title f-primary-sb"></span>--}}


{{--                                <div class="f-infoblock-with-icon__info_text b-infoblock-with-icon__info_text">--}}

{{--                                </div>--}}

{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-4 col-sm-12 col-xs-12">--}}
{{--                        <div class="b-infoblock-with-icon">--}}
{{--                            <div class="b-infoblock-with-icon__info">--}}

{{--                                <span--}}
{{--                                    class="f-infoblock-with-icon__info_title b-infoblock-with-icon__info_title f-primary-sb"></span>--}}


{{--                                <div class="f-infoblock-with-icon__info_text b-infoblock-with-icon__info_text">--}}

{{--                                </div>--}}

{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}

            </div>
        </section>


    </div>
</div>
{{--<div id="c862" class="csc-default">--}}
{{--    <div class="tx-dce-pi1">--}}

{{--        <section class="b-section-info">--}}
{{--            <div class="container">--}}

{{--                <div class="row">--}}


{{--                    <div class="col-md-6">--}}

{{--                        <img class="b-img-responsive" src="{{asset('files/We-will-find.jpg')}}"--}}
{{--                             alt="SHAGA MIX: Efficient recruiting expertise at your side"/>--}}

{{--                    </div>--}}
{{--                    <div class="col-md-6">--}}
{{--                        <h2>Efficient recruiting expertise at your side</h2>--}}
{{--                        <p class="f-primary subline-description">If you leave the time-consuming personnel work to us,--}}
{{--                            you will profit from our recruitment expertise and will reduce the recruitment expenses to a--}}
{{--                            minimum.--}}

{{--                            Recruitung process after your personal inquiry:</p>--}}

{{--                        <ul class="c-primary icon-list b-list-markers f-list-markers b-list-markers--without-leftindent f-list-markers--medium c-primary--all b-list-markers-2col f-list-markers-2col">--}}

{{--                            <li><i class="circle-numer">1</i> Analysis and creation of a requirements profile</li>--}}

{{--                            <li><i class="circle-numer">2</i> Cross-media candidate search</li>--}}

{{--                            <li><i class="circle-numer">3</i> Administration of incoming applications</li>--}}

{{--                            <li><i class="circle-numer">4</i> Screening / matching</li>--}}

{{--                            <li><i class="circle-numer">5</i> Check of candidates through interviews and if necessary--}}
{{--                                qualification tests--}}
{{--                            </li>--}}

{{--                            <li><i class="circle-numer">6</i> Standardisation of the application documents to facilitate--}}
{{--                                comparison--}}
{{--                            </li>--}}

{{--                            <li><i class="circle-numer">7</i> Presentation of two or three custom-fit candidates</li>--}}

{{--                        </ul>--}}


{{--                    </div>--}}

{{--                </div>--}}
{{--            </div>--}}
{{--            <script>$(document).ready(function () {--}}
{{--                    $("#playButton862").click(function () {--}}
{{--                        $(this).parent().parent().parent().find("video").get(0).play();--}}
{{--                    });--}}
{{--                });</script>--}}
{{--        </section>--}}

{{--    </div>--}}
{{--</div>--}}
<div id="c864" class="csc-default">
{{--    <section class="b-infoblock--without-border">--}}
{{--        <div class="b-carousel-reset b-carousel-arr-out b-carousel-small-arr f-carousel-small-arr b-remaining">--}}
{{--            <div class="f-center b-logo-group j-logo-slider">--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/e-dynamics_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/e-dynamics_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/adp_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/adp_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/fev_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/fev_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/arvato_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/arvato_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/pan_amp_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/pan_amp_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/dmc_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/dmc_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/thuega_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/thuega_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/lekkerland_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/lekkerland_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/sectra_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/sectra_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/lyreco_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/lyreco_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/clarks_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/clarks_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/coty_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/coty_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/ea_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/ea_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/fresenius_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/fresenius_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/nedermann_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/nedermann_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/tmed_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/tmed_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/tedrive_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/tedrive_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/solarworld_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/solarworld_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/armstrong_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/armstrong_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/minimax_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/minimax_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/neoplan_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/neoplan_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/entega_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/entega_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/honda_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/honda_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/trifels_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/trifels_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/aggreko_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/aggreko_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/kombi_verkehr_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/kombi_verkehr_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/elwema_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/elwema_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/cemex_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/cemex_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/trianel_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/trianel_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/linklaters_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/linklaters_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/mader_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/mader_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/fujitsu_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/fujitsu_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="https://www.amadeus-fire.de/fuer-unternehmen/referenzen/kundenberichte/north-channel-bank/"--}}
{{--                       target="_blank">--}}

{{--                        <img class="is-normal" src="{{asset('files/Logo_North_Channel_Bank_sw.jpg')}}"--}}
{{--                             alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/Logo_North_Channel_Bank_sw.jpg')}}"--}}
{{--                             alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/vr_leasing_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/vr_leasing_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/nrw_bank_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/nrw_bank_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/firmenich_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/firmenich_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/continental_contitech_1c.jpg')}}"--}}
{{--                             alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/continental_contitech_1c.jpg')}}"--}}
{{--                             alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/dpd_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/dpd_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/hirschmann_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/hirschmann_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/mitsubishi_electric_1c.jpg')}}"--}}
{{--                             alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/mitsubishi_electric_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/sportfive_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/sportfive_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <div class="b-logo-item">--}}

{{--                    <a href="javascript:return false;">--}}

{{--                        <img class="is-normal" src="{{asset('files/silfox_1c.jpg')}}" alt="Logo"/>--}}
{{--                        <img class="is-hover" src="{{asset('files/silfox_1c.jpg')}}" alt="Logo"/>--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}
</div>
<div id="c867" class="csc-default">
    <div class="tx-dce-pi1">

        <section class="b-section-info faq-module" style="">
            <div class="container">
                <h1>Frequently asked questions (FAQs)</h1>
                <div class="row">
                    <div class="col-md-6 col-xs-12 cm-sidebar">


                        <div class="h4 f-primary-b">What benefits does our company have when working together with
                            SHAGA MIX?
                        </div>
                        <p>You can focus entirely on your core business by leaving us the complex and occasionally
                            short-term recruitment of your employees. Due to our consequent specialisation on the
                            Software Development, Marketing, Project Management  and IT-Services divisions, we ensure a qualified needs analysis
                            and a custom-fit filling of your vacancies. We are able to recruit qualified experts for you
                            within a few days. Further information…</p>


                        <div class="h4 f-primary-b">What costs we will incur?</div>
                        <p>Initially, no costs will incur as your payments to us are performance-based. If you appoint
                            one of our employees within the temporary staffing, the following costs will incur: – The
                            contractually agreed hourly wage for the borrowed employee; – If your company belongs to one
                            of the following industries: metal and electro, railway, chemistry, rubber and plastcics,
                            you will have to pay industry surcharges; – If you want to engage our appointed employee
                            permanently before a period of up to nine months after termination of the personnel leasing
                            or within the personnel leasing, a commision fee will accrue. The commision fee measures
                            according to the duration of the personnel leasing.</p>


{{--                        <div class="h4 f-primary-b">On which industries and professions is SHAGA MIX--}}
{{--                            specialised?Berufe ist SHAGA MIX spezialisiert?--}}
{{--                        </div>--}}
{{--                        <p>We are specialised on the placement of experts and managers in the finance and accounting,--}}
{{--                            banking and all other commercial professional fields as well as in the IT-sector. We do not--}}
{{--                            have a specific industry focus.</p>--}}


{{--                        <div class="h4 f-primary-b">Are there any references from customer companies?</div>--}}
{{--                        <p>Yes. Please take a look at our references. You will find them here on our website.</p>--}}


                    </div>

                    <div class="col-md-6 col-xs-12 cm-sidebar">


                        <div class="h4 f-primary-b">Why should we engage SHAGA MIX?
                        </div>
                        <p>You should engage us because, as a specialist for experts an managers from the commercial
                            sphere and IT-sector, we can provide you custom-fit candidates. In the collaboration with
                            our customer companies, we aim to establish a long-term, strategic partnership that ensures
                            decisive competitive advantages in the personnel planning and recruitment for those
                            companies. Due to our complementary service portfolio, we are able to offer you
                            supplemantary training and education opportunites.</p>


{{--                        <div class="h4 f-primary-b">In what regions is SHAGA MIX present?</div>--}}
{{--                        <p>We cover the entire federal territory with our nationwide 19 branches. The regional proximity--}}
{{--                            to our customers, employees and candidates is really important to us and enables the--}}
{{--                            personal contact and therefore a trustful cooperation.</p>--}}


{{--                        <div class="h4 f-primary-b">What happens when the staffing does not work during the probation--}}
{{--                            period?--}}
{{--                        </div>--}}
{{--                        <p>Unfortunately, a guarantee for these cases does not exist even if you recruit without the--}}
{{--                            support of a personnel service company. In these cases, we certainly try to fill the vacancy--}}
{{--                            with another employee as soon as possible.</p>--}}


                    </div>
                </div>

            </div>
        </section>

    </div>
</div>
{{--<div id="c868" class="csc-default">--}}
{{--    <div class="tx-dce-pi1">--}}

{{--        <section class="crosslink-module crosslink-boxes">--}}
{{--            <div class="b-employee">--}}
{{--                <div class="container">--}}
{{--                    <h2 class="f-center">Find out more about us</h2>--}}
{{--                    <p class="f-center">You will find all the important information about our specialised personnel--}}
{{--                        services for applicants and companies here on our website. You prefer the personal conversation?--}}
{{--                        Do not hesitate to call us! Our nationwide 19 branches are close to you.</p>--}}
{{--                    <div class="b-employee-container row">--}}

{{--                        <div class="col-md-4 col-sm-6 col-xs-12">--}}
{{--                            <div class="col-md-10 col-sm-12 col-xs-12 col-centered col-nopad">--}}

{{--                                <div class="b-employee-item b-employee-item--color f-employee-item">--}}
{{--                                    <div class=" view view-sixth">--}}
{{--                                        <a href="{{route('index').'#contact_part'}}"><img class="j-data-element"--}}
{{--                                                                                    data-animate="fadeInDown"--}}
{{--                                                                                    data-retina--}}
{{--                                                                                    src="{{asset('files/hi_27.png')}}"--}}
{{--                                                                                    alt="Contact us"/></a>--}}
{{--                                        <div class="view-mask"></div>--}}
{{--                                    </div>--}}
{{--                                    <div style="height:5em;padding:10px;"><p style="height:100%;color: #353436;"--}}
{{--                                                                             class="h4">Contact us</p></div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="clearfix visible-sm-block"></div>--}}

{{--                        <div class="col-md-4 col-sm-6 col-xs-12">--}}
{{--                            <div class="col-md-10 col-sm-12 col-xs-12 col-centered col-nopad">--}}


{{--                            </div>--}}
{{--                        </div>--}}

{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </section>--}}

{{--    </div>--}}
{{--</div>--}}

@endsection
@section('js')
    @endsection
<!-- Cached page generated 22-03-20 15:39. Expires 23-03-20 15:39 -->
<!-- Parsetime: 0ms -->
